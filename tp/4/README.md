# TP4 : SaaS

L'objectif de ce TP est de monter votre propre offre SaaS.

Pour rappel, un SaaS, ou *Software as a Service* est la fourniture d'un service à traver un internet, directement aux utilisateurs finaux.

On peut par exemple citer Netflix, Youtube, Google Drive, Office 365, etc. Toutes ces apps sont considérées comme des offres SaaS.

**Votre SaaS sera une application de votre choix (streaming audio, wiki, etc.).**

**Tous les déploiements doivent user et abuser d'automatisation, et vous utiliserez des outils d'Infrastructure as Code.**  
Les outils à votre disposition que l'on abordé/mentionné :
* Provisionning de machines 
  * Terraform + plugin OpenNebula (recommandé)
  * Vagrant + plugin OpenNebula
* Configuraton
  * Ansible
* Déploiement
  * Docker + `docker-compose`
  * Swarm/Kubernetes

Le but est donc de pouvoir lancer une application donnée en quelques clics/commandes, en partant d'une plateforme de cloud computing comme OpenNebula (cloud privé) jusqu'à la fourniture du service final, sous la forme d'une IP qui donne sur une interface Web.

# Sommaire


<!-- vim-markdown-toc GitLab -->

* [I. Setup](#i-setup)
    * [0. Base](#0-base)
    * [1. Environnement](#1-environnement)
    * [2. Provision](#2-provision)
    * [3. Automate](#3-automate)
    * [4. Deploy](#4-deploy)
* [II. Bonus](#ii-bonus)

<!-- vim-markdown-toc -->

# I. Setup

## 0. Base

Toutes vos machines devront :
* utiliser CentOS7 ou CentOS8 (votre choix)
* avoir un hostname défini
* connaître le nom des autres machines (fichiers `/etc/hosts`)
* avoir un firewall activé et configuré
* SELinux désactivé (mode *permissive*)

> Ne vous embêtez pas avec la gestion des clés SSH tout au long de ce TP. Vous pouvez les générer en amont, et les déposer là où c'est nécessaire (plutôt que générer à la volée, déposer dynamiquement, etc.).

## 1. Environnement

Le setup se fera sur les machines de l'école. 

MP moi pour que je vous provisionne des machines si besoin, on verra dans un second temps pour un provisionnement automatisé.

Les OS à disposition sont CentOS7 et CentOS8.

## 2. Provision

Vous devrez mettre en place une plateforme OpenNebula à trois noeuds, et un quatrième noeud qui portera le frontend.

Pour ce faire, vous pouvez réutiliser vos playbooks Ansible afin de déployer la plateforme.

## 3. Automate

Une fois OpenNebula en place, vous pouvez créer des machines virtuelles. Faites en sorte d'avoir un accès SSH à ces VMs.

La configuration des machines virtuelles se fera à l'aide d'Ansible. 

> Lorsque vous créez une machine CentOS7 ou CentOS8 (par exemple) en vous servant des images du MarketPlace OpenNebula, vous pouvez instancier la VM avec une clé publique SSH déjà déposée.

## 4. Deploy

Une fois la configuration des VMs effectuées, on va pouvoir déployer une application dessus.

Déployez une application n-tiers à l'aide d'Ansible et/ou de Docker.

> Il est possible d'aller jusqu'à déployer un Swarm ou un Kubernetes automatiquement à cette étape, pour nos déploiements applicatifs.

Notez qu'OpenNebula possède un concept de *template* qui permet d'utiliser une VM comme "patron" pour en créer d'autres par la suite avec de la configuraiton pré-effectuée, accélérant les déploiements.

# II. Bonus

Quelques objectifs bonus, pour améliorer le niveau de qualité de la plateforme :
* un stockage partagé entre les noeuds 
* monitoring des noeuds et/ou des machines virtuelles crées sur la plateforme
