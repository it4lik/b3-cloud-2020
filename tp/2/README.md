# TP2 : Orchestration de conteneurs et environnements flexibles

Dans ce TP, on va approfondir les notions liées aux environnements flexibles que l'on retrouve dans le *cloud*.  

Par environnements flexibles, comme dans le *cloud*, on entend un environnement :
* avec des ressources créées à la demande
* une scalabilité horizontale
* une gestion orchestrée des services
* des outils intégrés
  * monitoring
  * backup
  * gestion du réseau
  * etc.

Les infrastructures modernes, orientées sur les services fournis, essaient de mettre en valeur les caractéristiques suivantes :
* Infrastructure as Code et automatisation
  * avec des outils comme Vagrant ou Terraform par exemple pour la création des machines
  * et Docker, docker-compose, et autres Ansible/Puppet pour le déploiement des services
  * cela nous amène une **répétabilité des déploiements**, ainsi qu'une **conformité** et une **hausse de qualité et de sécurité**
  * utilisation de `git`
* écosystème riche
  * il existe désormais de nombreuses solutions intégrées à ces environnements
  * chaque fonction est remplie par une application dédiée
  * monitoring, récolte de logs, proxying, etc.
* déploiements facilités, aisés
  * déploiements simples, rapides et scalables
  * méthode de packaging unifiée (conteneurs, Infrastructure as Code)
* robustesse
  * redondance (applications, stockage, etc.)
  * applications hautement disponibles

---

Ce TP a pour objectif de vous faire monter un environnement orchestré (Docker Swarm) avec des outils intégrés. Petite liste des technos au menu :
* Docker (packaging, déploiement), docker-compose (déploiement), Docker Swarm (orchestration)
* Vagrant (déploiement)
* Traefik (cloud-native reverse proxy)
* Swarmpit (WebUI par dessus Docker Swarm)
* Minio (S3 storage)
* Restic (backup)
* Weave (monitoring)
* Prometheus (monitoring)
* Registry

# Sommaire

<!-- vim-markdown-toc GitLab -->

* [I. Lab setup](#i-lab-setup)
* [II. Mise en place de Docker Swarm](#ii-mise-en-place-de-docker-swarm)
    * [1. Setup](#1-setup)
    * [2. Une première application orchestrée](#2-une-première-application-orchestrée)
* [III. Construction de l'écosystème](#iii-construction-de-lécosystème)
    * [1. Registre](#1-registre)
    * [2. Centraliser l'accès aux services](#2-centraliser-laccès-aux-services)
    * [3. Swarm Management WebUI](#3-swarm-management-webui)
    * [4. Stockage S3](#4-stockage-s3)
    * [5. Sauvegardes](#5-sauvegardes)
    * [6. Monitoring](#6-monitoring)
        * [A. SaaS : Weave Cloud](#a-saas-weave-cloud)
        * [B. HomeMade : Prometheus](#b-homemade-prometheus)

<!-- vim-markdown-toc -->

# I. Lab setup

| Noeud      | Rôle                 | Stockage                                |
|------------|----------------------|-----------------------------------------|
| `node1.b3` | Docker Swarm Manager | 1 disque système + 1 disque de données  |
| `node2.b3` | Docker Swarm Manager | 1 disque système + 1 disque de données  |
| `node3.b3` | Docker Swarm Manager | 1 disque système + 2 disques de données |

Afin de faciliter les déploiements, et de coller à la thématique de déploiement automatisé, **vous utiliserez Vagrant pour le déploiement des machines virtuelles**.  

Mettez en place 3 machines : 
* CentOS7 ou CentOS8
* stockage
  * un disque supplémentaire (peu importe la taille, quelques Go suffiront) sur toutes les machines
  * sur `node3`, ajouter un troisième disque. Quelques Go aussi suffiront.
* réseau
  * une carte accès internet
  * une carte accès à un LAN
  * hostname défini
* SELinux désactivé
* le firewall devra être activé et configuré
* fichier `/etc/hosts` rempli avec les hostnames des autres noeuds

Vous aurez aussi besoin de :
* `docker`, `docker-compose` installés
* le disque supplémentaire formaté et prêt à être monté

On va déployer plusieurs applications dans ce TP. Créez un répertoire de travail (un `/data` à la racine par exemple) où vous hébergerez vos applications (et leurs `docker-compose.yml` respectifs).

# II. Mise en place de Docker Swarm

## 1. Setup 

Swarm est un outil Docker natif. Il permet **une orchestration de services conteneurisés** avec Docker.

Au sein d'un cluster swarm, il existe deux rôles différents :
* les *managers*
  * ils sont utilisés pour maintenir le cluster en vie
  * ils peuvent faire tourner des services
* les *workers*
  * ils ne sont destinés qu'à faire tourner des services
  * ils reçoivent des tâches des *managers*
  * une tâche est le lancement d'un conteneur avec un contexte précis (stockage, réseau, etc.)

---

🌞 Utilisez des commandes `docker` afin de créer votre cluster Swarm
* `docker swarm init` pour créer le swarm
  * utilisez le `--advertise-addr` pour préciser l'adresse dans le LAN
* `docker swarm join` pour qu'un noeud rejoigne un swarm existant
* **vos 3 machines doivent être des *managers Swarm***

```
# Sur un premier noeud
docker swarm init --advertise-addr <LAN_IP>

# Obtenir un token pour ajouter des workers au cluster
docker swarm join-token worker

# Obtenir un token pour ajouter des managerss au cluster
docker swarm join-token manager

# Sur les autres noeuds
docker swarm join ... # réutiliser la commande obtenue avec le 'join-token'
```

---

Une fois en place, vous avez accès à de nouvelles commandes Docker, principalement :
* `docker node`
  * `docker node ls` permet de lister les noeuds du cluster
* `docker stack`
  * permet de manipuler des *stacks Docker*
  * une *stack* est un ensemble de services
  * `docker stack ls` pour lister les stacks en place (aucune pour le moment)

## 2. Une première application orchestrée

Afin de déployer des services sur un Swarm, on utilise des fichiers `docker-compose.yml`.

> Il est impératif d'utiliser des `docker-compose.yml` en version > 3.0 pour utiliser le fichier pour déployer dans un Swarm.

Le déploiement d'un `docker-compose.yml` dans un Swarm créera une *stack*. On peut déployer une nouvelle *stack* avec : 
```
$ docker stack deploy -c /path/to/docker-compose.yml stack_name
```

Vous pouvez obtenir des infos sur une *stack* qui tourne, notamment :
```
# Liste les stacks
docker stack ls

# Liste les services d'une stack
docker stack services <STACK_NAME>

# Récupère les logs d'un service
docker service logs <SERVICE_NAME>

# Récupère des infos sur un service
docker service ps <SERVICE_NAME>
```

> **NB** : Par défaut, si une application partage un port, alors toutes les machines du cluster Swarm ouvriront ce port.

---

🌞 "swarmiser" l'application Python du TP1 
* récupérez le `docker-compose.yml` que vous aviez écrit et le déployer dans le Swarm
  * **l'image doit être présente sur tous les noeuds du cluster**
    * vous devez donc la build sur tous les noeuds du cluster
      * `docker build` du Dockerfile **sur les 3 machines**
  * utiliser un partage de ports pour accéder à l'application
    * pour rappel, elle écoute sur le port 8888/TCP
    * le partage est à ajouter dans le `docker-compose.yml` avec la clause `ports`
    * le but est de permettre à l'application d'être accessible depuis l'extérieur, en utilisant l'IP de l'hôte

🌞 explorer l'application et les fonctionnalités de Swarm
* tester le bon fonctionnement de l'application
* utiliser `ss` sur tous les noeuds pour comprendre sur quel port est exposée l'application
  * `sudo ss -plant` pour avoir une liste de tous les ports TCP en écoute
* utiliser `docker service scale` afin d'augmenter le nombre de services Python
  * `docker service scale -h` pour plus d'infos
  * vérifier que c'est ok en visitant l'interface web (l'id du conteneur est print sur la page web)
  * avec un navigateur ou une commande `curl`
* trouver sur quel(s) hôte(s) tournent tous les conteneurs lancés
* faire des tests de bascule en coupant Docker/éteignant une machine

> Vous pouvez utiliser des [`restart_policy`](https://docs.docker.com/compose/compose-file/#restart_policy) dans vos `docker-compose.yml` pour choisir comment vos conteneurs doivent redémarrer.

# III. Construction de l'écosystème

## 1. Registre

Un **registre d'image** Docker est une application permettant de **stocker et distribuer des images** Docker. Le Docker Hub est un registre Docker public.

🌞 Déployer un registre Docker simple
* déployez sur `node1` avec une commande `docker stack`
  * utilisez [le `docker-compose.yml` fourni](./registry/docker-compose.yml)
  * il faudra créer les trois répertoires renseignés à l'intérieur
* configurer les fichier hosts de vos noeuds pour qu'ils puissent joindre le registre sur `registry.b3` (IP de node1 qui pointe vers `registry.b3`)
* modifier la configuration de votre démon docker pour ajouter l'option `insecure-registries`
  * car on utilise pas de HTTPS ici

---

**Vous pouvez alors pousser** (`docker push`) et **récupérer** (`docker pull`) des images vers/depuis votre registre :

```
# Récupération d'une image extérieure
docker pull alpine

# Renommage
docker tag <NAME> <REGISTRY>/<REPO_NAME>/<IMAGE_NAME>:<TAG>
docker tag alpine registry.b3:5000/alpine/alpine:test

# Push
docker push registry.b3:5000/alpine/alpine:test

# Récupération 
docker pull registry.b3:5000/alpine/alpine:test

# Et/ou utilisation
docker run -it registry.b3:5000/alpine/alpine:test sh
```

🌞 Tester le registre et utiliser le registre
* build l'image contenant l'app Python sur un noeud, en la nommant correctement pour notre registre
* pousser l'image de l'application Python
* adapter le `docker-compose.yml` de l'application Python pour utiliser l'image du registre

> Dans un cas réel, on peut répliquer le registre. Pour qu'il soit répliqué, il est nécessaire que les hôtes du cluster aient tous accès à un stockage partagé (NFS, Windows DFS, CEPH, etc.) afin de partager les données qu'ils stockent. Ainsi, peu importe le conteneur 'registre' que l'on joint, ils partagent tous le même stockage.

## 2. Centraliser l'accès aux services

Actuellement, un service web ne peut être exposé que directement en ouvrant un port sur tous les noeuds du cluster.  

Dans cette partie, vous mettre en place [Traefik](https://docs.traefik.io/) afin de gérer les accès aux services Web qui tournent dans le cluster. 

> Coupez la stack Python lancée dans la précédente partie pour la suite. 

**Traefik est un reverse proxy** dynamique, qui s'intègre nativement avec Swarm. Il est **capable de détecter des applications** qui sont créées dans le Swarm, et de modifier automatiquement sa configuration afin de les servir. 

> Ce TP ayant pour but de vous faire explorer ces environnements Cloud orchestrés, on se cantonnera à une utilisation simple de Traefik. 

---

🌞 **Déployer une *stack* Traefik**
* créer un réseau dédié à Traefik
  * il sera utilisé pour Traefik
  * toutes les applications qui auront besoin du reverse proxy Traefik seront aussi dans ce réseau

```
# Création du réseau
docker network create --driver overlay traefik

# Vérifier la création. Le réseau doit être présent sur tous les noeuds du cluster
docker network ls
```

🌞 **Déployer une *stack* Traefik**
* utiliser [le `docker-compose.yml` fourni](./traefik/docker-compose.yml)
  * noter l'utilisation du réseau `traefik` en `external` 
    * cela indique qu'il doit être créé auparavant
    * ce réseau sera aussi utilisé pour les prochaines applications que l'on mettra derrière le reverse proxy
  * modifier le fichier pour définir un nom de domaine sur lequel sera accessible le dashboard Traefik
    * le `<DASHBOARD_URL>` dans le fichier [`docker-compose.yml`](./traefik/docker-compose.yml)
    * ça peut être n'importe quel nom `nom.de.domaine`
    * ce nom doit être ajouté à votre fichier `hosts` et doit pointer vers n'importe quel hôte du cluster
* vérifier le bon fonctionnement en visitant l'interface web de Traefik
  * vos **devez** joindre les applications derrière Traefik avec leurs nom de domaine
  * `http://<DASHBOARD_URL>` dans votre navigaeur (observez la redirection)

---

🌞 **Passer l'application Web Python derrière Traefik**
* réutiliser encore le `docker.compose.yml` de l'app Python
  * positionner des labels dans le `docker-compose.yml` afin que Traefik détecte l'application
  * **enlever le partage de ports** que vous aviez mis
  * préciser l'utilisation du réseau `traefik` en `external` (voir un peu plus bas)
* l'application sera accessible *via* un nom de domaine 
  * le `<DOMAIN_NAME>` choisi dans l'exemple ci-dessous
  * sans serveur DNS, **il faudra ajouter le nom à votre fichier hosts** (celui de la machine qui effectue le test)
* tester
  * avec un `curl` ou un navigateur web depuis votre poste

Labels à positionner dans le `docker-compose.yml` : 
```
# Ajout de la clause deploy, dans un service
services:
  example:
    networks:
      traefik:
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.<SERVICE_NAME>.rule=Host(`<DOMAIN_NAME>`)"
        - "traefik.http.routers.<SERVICE_NAME>.entrypoints=web"
        - "traefik.http.services.<SERVICE_NAME>.loadbalancer.server.port=<SERVICE_PORT>"
        - "traefik.http.routers.<SERVICE_NAME>-tls.rule=Host(`<DOMAIN_NAME>`)"
        - "traefik.http.routers.<SERVICE_NAME>-tls.entrypoints=webtls"
        - "traefik.http.routers.<SERVICE_NAME>-tls.tls.certresolver=letsencryptresolver"
networks:
  traefik:
    external: true

# Exemple
services:
  python-webapp:
    networks:
      traefik:
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.python-webapp.rule=Host(`python.webapp.tp3`)"
        - "traefik.http.routers.python-webapp.entrypoints=web"
        - "traefik.http.services.python-webapp.loadbalancer.server.port=8888"
        - "traefik.http.routers.python-webapp-tls.rule=Host(`python.webapp.tp3`)"  
        - "traefik.http.routers.python-webapp-tls.entrypoints=webtls"
        - "traefik.http.routers.python-webapp-tls.tls.certresolver=letsencryptresolver"
```

> Attention dans "Host(`python.webapp.tp3`)" ce sont des **backticks**, pas des simples quotes.

## 3. Swarm Management WebUI

On va utiliser l'outil [SwarmPit](https://github.com/swarmpit/swarmpit) afin d'avoir un dashboard sur le fonctionnement du cluster Swarm.

Allez jeter un oeil à la doc officielle pour le déployer.

Son interface doit être accessible derrière Traefik.

**Il faudra donc modifier le `docker-compose.yml` fourni dans la doc** :
* utilisation des labels pour Traefik
* absence de partage de port
* ajout du réseau `traefik` en external
* le conteneur qui expose l'interface web doit être dans ce réseau `traefik`

## 4. Stockage S3

> Note : Lisez en entier avant de vous lancer.

**S3 est devenu *de facto* un standard pour le stockage dans des environnements cloud**. S3 doit son nom à l'offre de services [Amazon S3](https://aws.amazon.com/fr/s3/), qui propose du stockage *via* une API HTTP.  

L'API étant très utilisée, elle est devenue un outil très commun au sein des environnements Cloud. Ainsi, d'autres solutions ont vu le jour, d'autres solutions qui implémente cette même API S3.  

C'est le cas de [Minio](https://min.io/) que nous allons mettre en place. 

---

Minio sera lancé comme une stack dans le cluster Swarm.  
Minio est une solution de stockage, on doit lui fournir un endroit où stocker ses données. **Afin de ne pas perturber l'instance Minio** à travers les redémarrage (reboot machine, redémarrage docker, etc), **on va faire en sorte que les conteneurs Minio se lancent respectivement toujours au même endroit** grâce à des labels Swarm. 

| Conteneur | Hôte       | Data Path  |
|-----------|------------|------------|
| `minio1`  | `node1.b3` | `/minio`   |
| `minio2`  | `node2.b3` | `/minio`   |
| `minio3`  | `node3.b3` | `/minio`   |
| `minio4`  | `node3.b3` | `/minio-2` |

🌞 **Préparer l'environnement**
* utiliser LVM pour partitionner les disques supplémentaires ajoutés aux VMs

---

🌞 **Déployer Minio**
* [déployer Minio en utilisant le `docker-compose.yml` de la doc](https://docs.min.io/docs/deploy-minio-on-docker-swarm)
  * pour la partie avec les labels de noeuds, nous, on a que trois noeuds, donc :
  * l'interface de Minio doit être disponible à travers Traefik

```
docker node update --label-add minio1=true <DOCKER-NODE1>
docker node update --label-add minio2=true <DOCKER-NODE2>

# Puis deux fois le node3
docker node update --label-add minio3=true <DOCKER-NODE3>
docker node update --label-add minio4=true <DOCKER-NODE3>
```

🌞 **Déployer Minio** (suite)
* dans le `docker-compose.yml`
  * ne pas exposer Minio avec un partage de port
    * enlever le partage de ports du `docker-compose.yml`
  * exposer Minio à travers Traefik 
    * ajouter des labels dans le `docker-compose.yml`
    * il faut aussi ajouter les conteneurs minio dans le réseau créé pour Traefik, pour que ce dernier puisse les joindre
    * Minio expose son interface Web sur le port 9000/TCP
* créer un bucket sur l'interface Web afin de tester le bon fonctionnement

---

🌞 **Tester Minio**
* utiliser [s3fs](https://github.com/s3fs-fuse/s3fs-fuse) pour monter votre bucket comme une partition

> Vous pouvez aussi tester [JuiceFS](https://juicefs.com) que je connais peu mais qui a l'air très prometteur.

Dans le cas d'une utilisation réelle, Minio peut être positionné sur des serveurs dédiés au stockage afin d'en maximiser les performances. 

--- 

Maintenant qu'on a un stockage partagé entre les trois machines avec Minio + s3fs, on peut y héberger nos configurations. 

🌞 **Héberger les configurations dans Minio**
* créer un bucket dédié aux configurations des applications swarm, on y stockera :
  * tous les `docker-compose.yml` 
  * éventuellement des données qui sont liées aux apps, comme des fichiers de conf
* monter le bucket sur `/data` sur tous les noeuds
* y stocker les applications du Swarm (les fichiers `docker-compose.yml`
  * ce dossier devra figurer dans le rendu

## 5. Sauvegardes

Le but de cette partie est de mettre en place un outil de sauvegarde avec pour destination un bucket S3 (dans Minio). 

Vous pouvez utiliser l'outil de votre choix, je vous recommande [restic](https://github.com/restic/restic).

🌞 **Backup S3 avec restic**
* faites une installation de restic en suivant la [doc officielle](https://restic.readthedocs.io/)

🌞 **Backup S3 avec restic**
* créer des sauvegardes d'un répertoire de votre choix (données du registre Docker mis en place au début par exemple)
* [tester d'abord à la main](https://restic.readthedocs.io/en/latest/030_preparing_a_new_repo.html#minio-server) les backups dans un bucket S3 dédié
* puis utiliser un timer systemd afin de lancer les backups de façon périodique
  * il vous faudra créer un fichier `.service` systemd
  * et un fichier `.timer` systemd
  * tell me si besoin d'aide sur ça
* tester une restauration

> Dans Minio, il est possible de gérer des droits. Question de sécurité, il serait intéressant de créer un compte dédié à restic, qui n'a uniquement accès à son bucket pour les sauvegardes.

## 6. Monitoring

### A. SaaS : Weave Cloud

Créez vous un compte sur [Weave Cloud](https://cloud.weave.works/signup).
* puis choisissez de créer une connexion à un cluster
* choisissez bien Docker Swarm (pas Docker tout court)
* lancer la commande `docker run` que l'on vous fournit (avec le toke associé à votre compte)
  * ce conteneur va lancer une stack sur votre cluster

🌞 **Explorer Weave**
* déterminer les conteneurs lancés par Weave
* déterminer le rôle de chacun d'entre eux

> Baladez vous un peu sur la WebUI (accessible une fois en ligne sur le Weave Cloud, une fois votre cluster connecté).

### B. HomeMade : Prometheus

**Prometheus est un outil utilisé pour analyser des données et métriques**. On peut ainsi s'en servir pour monitorer un parc de machines. Il est **particulièrement adapté aux environnements cloud** (c'est un projet soutenu par la [CNCF](https://www.cncf.io/).

Prometheus n'est que l'entité qui stocke et permet d'effectuer des requêtes sur des données. **Pour l'exploiter pleinement, on ajoute souvent** : 
* **des récolteurs de données**
  * installés sur les noeuds, ils récoltent les différentes métriques de l'hôte
  * ils exportent les données vers Prometheus
* un outil qui permet de **visualiser les données**
  * afin de mieux exploiter les données de Prometheus, il peut être intéressant de créer des dashboards pour visualiser ces données

---

Pour que le setup soit plus rapide, il existe déjà des travaux qui ont été faits pour lier une stack avec Prometeus à un Swarm Docker. On va ici utiliser [swarmprom](https://github.com/stefanprodan/swarmprom).


🌞 **Déployer Swarmprom**
* expliquer l'utilité des différents composants de swarmprom (autres que Prometheus)
* mettre le dashboard tous les services web derrière Traefik
  * il y a déjà un `docker-compose.traefik.yml` dans le dépôt swarmprom
* aucun partage de ports ne doit être utilisé (uniquement le reverse proxy Traefik qui rend accessible les interfaces Web)
* vous pouvez utiliser les alertes Slack si vous avez un channel (ça se crée vite fait avec un compte sinon :) ) mais rien d'obligatoire !

> **Le principal Dashboard est disponible sur Grafana !**
