# TP Cloud

* [TP1 : Conteneurisation](./1)
* [TP2 : Orchestration de conteneurs et environnements flexibles](./2)
* [TP3 : Mise en place d'une plateforme de cloud privé](./3)
* [TP4 : Déploiement automatisé d'un SaaS](./4)

# Pendant les TPs...

* le but est de vous faire pratiquer. N'hésitez pas à dériver sur des choses qui vous intéressent un peu plus, en restant sur le thème du TP. Si c'est ennuyant, rendez le truc intéressant pour vous
* faites vos recherches en anglais, mettez votre OS en anglais (au moins vos VMs)
* n'hésitez pas à me MP Discord ou IRL en cas de besoin
* préférez les copier/coller aux screens

**Allez à votre rythme, prenez le temps de comprendre.**  
**Posez des questions.**  
**Prenez des notes au fur et à mesure.**  
**Lisez les parties en entier avant de commencer à travailler dessus.**  
**Smile** 🌞

## Rendu de TP

* format **Markdown uniquement**
* **via un dépôt `git`** (plateforme de votre choix, Github, Gitlab, autres)
* soyez complets et clairs
  * souvent un copier/coller de la ligne de commande et une petite ligne d'explication suffisent
* sachez aussi être concis
  * ça m'intéresse pas forcément d'avoir TOUTES les étapes de vos manipulations sauf quand explicitement demandé
* **les éléments précédés d'un 🌞 doivent absolument figurer dans le rendu**. Si c'est dans une bullet-list, tous les éléments doivent être rendus. Dans l'exemple qui suit, détail 1 et détail 2 doivent aussi figurer sur le rendu :
```
* 🌞 Objectif global
  * détail 1
  * détail 2
```

* **les TPs sont à rendre *via* MP sur discord avec la chaîne `NOM,URL` et précisez votre promo**
  * c'est le nom pas le prénom
  * l'`URL` doit pointer vers votre rendu directement, ou il doit être simple à trouver (une page avec tous les rendus et des liens vers les rendus c'est OK du moment que c'est clair)
  * si la chaîne `NOM,URL` est mal formatée je prends pas, vous êtes grands now
