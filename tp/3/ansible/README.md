# TP 3.5 : Ansible introduction

<!-- vim-markdown-toc GitLab -->

* [Intro](#intro)
* [0. Prérequis](#0-prérequis)
* [1. First steps](#1-first-steps)
* [2. Your own playbooks](#2-your-own-playbooks)
    * [A. Database](#a-database)
    * [B. Users](#b-users)

<!-- vim-markdown-toc -->

# Intro

Ansible est un outil de **gestion et déploiement de configurations**. Il est libre et opensource, et est développé en Python.  
C'est un des outils les plus en vogue en terme de gestion d'infrastructure.  
Bien qu'adaptables à d'autres types d'équipements/systèmes, Ansible s'intègre extrêmement bien aux parcs GNU/Linux.

Il fonctionne sur un **principe déclaratif : on écrit (on "décrit") dans des fichiers l'état dans lequel on souhaite que les machines soient**. Ansible s'occupera alors d'effectuer les opérations pour que la machine soit dans un état conforme.

Les configurations sont stockées dans des fichiers au format `.yaml` (ou `.yml`).  
Elles sont déployées à distance sur les hôtes grâce au protocole SSH.

---

**Un peu de vocabulaire** lié à Ansible :
* *inventory*
  * l'inventaire est **la liste des hôtes**
  * les hôtes peuvent être groupés dans un groupe qui porte un nom
  * un hôte c'est ici une machine qui recevera de la configuration
* *task*
  * une tâche est **une opération de configuration**
  * par exemple : ajout d'un fichier, création d'un utilisateur, démarrage d'un service, etc.
* *role*
  * un rôle est **un ensemble de tâches qui a un but précis**
  * par exemple :
    * un role "apache" : il installe Apache, le configure, et le lance
    * un role "users" : il déploie des utilisateurs sur les machines
    * il existe une plateforme communautaire pour partager des rôles : [Ansible Galaxy](https://galaxy.ansible.com/)
* *playbook*
  * un *playbook* est le lien entre l'inventaire et les rôles
  * un *playbook* décrit à quel noeud on attribue quel rôle

# 0. Prérequis

Pour ce TP, libre à vous de gérer votre environnement. De nouvelles machines (avec un petit Vagrantfile) sont conseillées, pour avoir un environnement clean où on peut faire plein de tests.

Machines :
* une machine qui sera appelée le *control node* : elle hébergera les configurations Ansible
* au moins une machine sur laquelle déployer de la configuration 

Configurations :
* SELinux désactivé
* `python` installé partout en dernière version
* `ansible` installé sur le *control node*
* un utilisateur dédié à ansible
  * présent sur toutes les machines
  * possède les droits *root* via `sudo` 
    * sans mot de passe pour faciliter les tests, c'est ok
  * il peut se connecter depuis le *control node* à toutes les autres machines en SSH
    * là aussi, en passwordless ça facilite les tests

# 1. First steps

Juste pour jouer, mettre en place Ansible et appréhender l'outil, on va rédiger un premier playbook dans un seul fichier.

L'exemple sera donné avec le déploiement d'un serveur web.

**Les opérations sont à réaliser avec l'utilisateur dédié créé pour Ansible, en vous connectant en SSH sur le *control node*.**  
**Créez un répertoire `ansible/` dans son répertoire personnel. TOUS les fichiers `.yml` que vous écrirez se trouveront à l'intérieur.**

* Créez un playbook minimaliste `nginx.yml` :
```yaml
---
- name: Install nginx
  hosts: ynov
  become: true

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install nginx
    yum:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started
```

Et créez un inventaire `hosts.ini` (en modifiant la valeur de `<VM_IP>` pour que ça corresponde à votre environnement) :
```
[ynov]
<VM_IP>
```

Enfin, créez un fichier `index.html.j2` :
```
Hello from {{ ansible_default_ipv4.address }}
```

Pour déployer cette configuration, il faudra utiliser la commande `ansible-playbook` :
```bash
$ ansible-playbook -i hosts.ini nginx.yml
```

**Une fois déployé, faites un test pour vous assurer que le serveur Web est fonctionnel.**

> Note : il est aussi possible d'exécuter de simples actions sur les machines du parc depuis la ligne de commande Ansible (sans forcément exécuter un playbook), c'est ce que Ansible appelle des [*ad-hoc commands*](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html). Très utile pour exécuter une action simple et ponctuelle sur toutes les machines du parc.

# 2. Your own playbooks

## A. Database

Créez un playbook qui :
* installe MariaDB
* lance MariaDB
* ajoute une base de données

## B. Users

Créez un playbook qui :
* ajoute un groupe
* ajoute 3 utilisateurs, et les place dans le groupe

And now...

<div align="center"><img src="./pics/automate-all-the-thing.jpg" /></div>
