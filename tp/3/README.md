# TP3 : Cloud Platform

<!-- vim-markdown-toc GFM -->

* [Intro](#intro)
* [0. Prerequisites](#0-prerequisites)
* [I. Setup](#i-setup)
    * [A. Présentation](#a-présentation)
    * [B. Mise en place](#b-mise-en-place)
        * [1. Frontend](#1-frontend)
        * [2. Virtu](#2-virtu)
        * [3. Test](#3-test)
* [I.5 : Entracte Ansible](#i5--entracte-ansible)
    * [1. Learn](#1-learn)
    * [2. Adapt](#2-adapt)
    * [3. Act](#3-act)
* [II. Going further](#ii-going-further)
    * [1. Ajouter un noeud](#1-ajouter-un-noeud)
    * [2. Automatiser le déploiement](#2-automatiser-le-déploiement)
    * [3. Partage de fichiers](#3-partage-de-fichiers)

<!-- vim-markdown-toc -->

# Intro

Moins guidé que les autres TPs, celui-ci a pour but de vous faire monter de toutes pièces une plateforme de cloud privé. 

Par cloud privé, on entend une plateforme de cloud-computing auto-hébergée. Le cloud computing c'est le fait de pouvoir réserver des ressources machines (CPU, RAM, Disque, Réseau, etc) et les consommer en s'abstrayant des contraintes physiques. 

Avec une plateforme de cloud computing, on peut provisionner des VMs, ou simplement exécuter des tâches mais on ne se souciera pas de l'aspect physique sous-jacent :
* pas d'IP à réserver ou de stockage à réserver
* accès aux ressources *via* le réseau, peu importe leur localisation
* mutualisation des ressources de l'infra, que le cloud consommera
* système de quotas

OpenNebula est une solution qui permet de mettre en oeuvre un cloud privé ou un cloud hybride.

# 0. Prerequisites

Deux VMs CentOS7 :

| Name         | RAM | CPU | IP 1            | IP 2            |
|--------------|-----|-----|-----------------|-----------------|
| frontend.tp3 | 4   | 1   | `10.100.100.11` | `10.110.110.11` |
| kvm1.tp3     | 4   | 2   | `10.100.100.12` | `10.110.110.12` |

Préparer les hosts :
* système à jour
* désactiver SELinux
* désactiver le firewall (dans un premier temps, vous le configurerez plus tard)
* noms de domaine définis et fichiers hosts remplis 

On utilise deux interfaces réseau :
* l'une sera utilisée pour le management
  * ce sont les IP1
  * "management" = c'est sur ces IPs qu'on se connecte en tant qu'admin
  * ce sont aussi ces IPs que les hôtes utilisent pour communiquer entre eux
* l'autre sera utilisée par les VMs
  * un bridge Linux sera utilisé (setup le plus simple) pour le réseau des VMs
  * ceci exige la présence d'une interface dédiée sur l'hôte
  * tell me si vous avez besoin d'aide sur le concept de bridge Linux

# I. Setup

## A. Présentation

> Tout ce qui est dit ici est une redite simplifiée de la documentation OpenNebula que je vous conseille de lire. En particulier [la présentation de la solution et de ses concepts](http://docs.opennebula.io/5.12/deployment/cloud_design/open_cloud_architecture.html)

Nous allons mettre en place OpenNebula (plateforme cloud) avec des hyperviseurs KVM (natifs Linux). Une machine sera le "frontend" : elle exposera l'interface graphique de gestion et portera le service `opennebula` qui gère toute la logique de la solution. Les autres machines seront des hyperviseurs KVM (une seule dans un premier temps).

Pour que la solution fonctionne, un utilisateur `oneadmin` est créé sur toutes les machines à l'installation des paquets, et un accès SSH sans password doit être possible entre toutes les machines du cluster.

Le réseau sera géré avec des bridges Linux. Le stockage sera géré avec des dossiers locaux. Les gestions réseau et stockage pourront être affinées, améliorées par la suite. **Le premier objectif est une mise en place simple et fonctionnelle.**

Pour qu'OpenNebula fonctionne, plusieurs services sont en jeu :
* `opennebula` : le coeur de la solution, c'est lui qui a la logique
* `opennebula-sunstone` : c'est l'interface web de gestion
* `kvm` : c'est l'hyperviseur

## B. Mise en place

### 1. Frontend

Configurez d'abord l'hôte `frontend.tp3` : [installation du coeur d'OpenNebula + l'interface graphique](http://docs.opennebula.io/5.12/deployment/opennebula_installation/index.html).

On utilisera SQLite (c'est à dire aucune mise en place) et pas une base de données dédiée.

🌞 Détaillez vos étapes (liste des commandes et éventuels fichiers de conf).

### 2. Virtu

Configurez le `kvm1.tp3` en suivant là encore [la documentation](http://docs.opennebula.io/5.12/deployment/node_installation/kvm_node_installation.html).

🌞 Détaillez vos étapes (liste des commandes et éventuels fichiers de conf).
* il faudra, entre autres, ajouter un bridge Linux sur l'hôte et configurer un Virtual Network dans l'interface Web de OpenNebula

### 3. Test

Depuis l'interface Web, ajoutez le nouveau node de virtu.

Importez une image du MarketPlace OpenNebula (CentOS7 ou CentOS8).

Editez le template de Machine Virtuelle pour y ajouter :
* votre clé publique SSH
* une carte réseau dans le VirtualNetwork défini plus tôt

Lancer une VM et essayez de vous y connecter. Il faudra ajouter votre clé publique au template de VM pour pouvoir y accéder, pas *via* mot de passe.

🌞 Pour tester le fonctionnement de la VM, utilisez le shell en vous y connectant en SSH, et essayez de ping l'hôte de virtu.

🌞 En ajoutant de la conf sur l'hôte, donnez un accès internet aux VMs.


# I.5 : Entracte Ansible

## 1. Learn

Se référer [au document dédié](./ansible/).

## 2. Adapt

🌞 Modifiez vos `Vagrantfile`s pour que des déploiements Ansible soit possible :
* utilisateur dédié 
* connexions SSH fonctionnelles
* Ansible et Python installés là où c'est nécessaire

L'idée c'est que, une fois vos machines Vagrant allumées, vous pouvez directement passer des commandes `ansible` et `ansible-playbook`.

> Tous vos fichiers Ansible, comme le Vagrantfile, devront figurer dans le rendu. Aussi, il est possible d'utiliser un "provisioner Ansible" avec Vagrant, de telle sorte à ce que Vagrant déploie automatiquement les *playbooks* Ansible après l'initialisation et démarrage de la machine.

## 3. Act

🌞 Ecrire un playbook Ansible qui installe le front de OpenNebula

🌞 Ecrire un playbook qui configure un noeud comme nouveau noeud de virtu pour le cluster OpenNebula

# II. Going further

## 1. Ajouter un noeud

🌞 Ajouter un noeud de virtu `kvm2.tp3` au cluster
* utiliser Ansible pour déployer tout ça
* tout doit être automatisé
  * y compris la configuration SSH

## 2. Automatiser le déploiement

Si on souhaite ajouter un nouveau noeud au cluster, il sera nécessaire de reconfigurer une nouvelle fois le noeud.

🌞 Mettre en place la configuration automatisée d'un hôte de virtu pour le cluster. Pensez notamment à :
* installation de paquets
* configurations
* gestion SSH

## 3. Partage de fichiers

Pour augmenter les performances du cluster, et la sécurité de ses données, il pourrait être intéressant de mettre en place un stockage partagé entre les hôtes de virtualisation. 

Ainsi, à la création d'une VM, ou encore au clonage, ou même lors de l'utilisation de fonctionnalités de haute-disponibilité, les hôtes de virtu du cluster auront accès en direct aux disques des machines, aux images disque `.iso`, etc.

🌞 Mettre en place un partage de fichiers entre les machines qui sont hôtes de virtu. Ce partage sera utilisé comme un [datastore d'images](https://docs.opennebula.io/5.12/operation/host_cluster_management/datastore_guide.html).
* libre à vous pour la façon de procéder ou le choix technique
* un partage NFS est ok
* un outil plus avancé comme [LizardFS](https://github.com/lizardfs/lizardfs) (nécessite plus de ressources)
