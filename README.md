# Cours Cloud YNOV B3 2020

* [Cours](./cours)
  * Docker
    * [Slides du cours](./docker/slides-containerization.html)
    * [Notion d'images Docker](./docker/images.md)
  * Vagrant
    * [Vagrant et repackaging](./vagrant/)
  * Ansible
    * [Notion de gestion de conf et présentation d'Ansible](./ansible/)
* [TP](./tp)
  * [Présentation des TPs](./tp)
  * [TP1 : Conteneurisation](./1)
  * [TP2 : Orchestration de conteneurs et environnements flexibles](./2)
  * [TP3 : Mise en place d'une plateforme de cloud privé](./3)
