# Ansible


<!-- vim-markdown-toc GitLab -->

* [I. Intro](#i-intro)
    * [1. Gestion et déploiement de configuration](#1-gestion-et-déploiement-de-configuration)
    * [2. Les outils disponibles](#2-les-outils-disponibles)
    * [3. Ansible](#3-ansible)
* [II. Why ?](#ii-why-)
    * [1. Pourquoi utiliser un outil de gestion de conf ?](#1-pourquoi-utiliser-un-outil-de-gestion-de-conf-)
    * [2. Pourquoi choisir Ansible ?](#2-pourquoi-choisir-ansible-)
    * [3. Des inconvénients ?](#3-des-inconvénients-)
    * [4. Infrastructure as code ?](#4-infrastructure-as-code-)
* [III. Ansible](#iii-ansible)
* [IV. Ansible good practices](#iv-ansible-good-practices)
    * [1. Gestion de conf de façon générale](#1-gestion-de-conf-de-façon-générale)
    * [2. Ansible](#2-ansible)
    * [3. Un mot sur le contrôle de conformité](#3-un-mot-sur-le-contrôle-de-conformité)

<!-- vim-markdown-toc -->

# I. Intro

## 1. Gestion et déploiement de configuration

Ansible est un outil de **gestion et de déploiement de configurations.**

Par ***gestion de conf***, on entend le **stockage des configurations, de façon centralisée**.

Par ***déploiement de conf***, on entend l'utilisation **d'un moyen unique et standard pour déployer les configuration** sur tout un parc.

Par ***conf***, on entend une définition très large : installation de paquets, démarrage de services, création d'utilisateurs, fichiers de configuration, etc.

Un tel outil permet donc **d'automatiser le déploiement** de configurations au sein d'un parc.  
En effet, on écrit une fois comment déployer une configuration donnée, et on peut par la suite la redéployer autant de fois que l'on veut.

## 2. Les outils disponibles

**[Ansible](https://www.ansible.com/)**, produit porté par RedHat, est l'outil de gestion de conf le plus en vogue en ce moment.  
Une grande communauté autour d'Ansible s'est formée, avec tous les bénéfices que cela peut apporter (retours d'expériences, rapports de bug, bonnes pratiques communes, fichiers Ansible rédigés par la communauté en libre accès, etc.).

Dans le monde de l'OpenSource, on peut aussi citer **[Puppet](https://puppet.com/)** qui est plus mature qu'Ansible et encore très utilisé.  
Il existe aussi **[Salt](https://saltproject.io/)** ou encore **[Chef](https://www.chef.io/)**.  
Dans le monde propriétaire, des outils dédiés à chaque plateforme existent, comme **[vRA](https://www.vmware.com/fr/products/vrealize-automation.html)** chez VMWare.

## 3. Ansible

Ansible, en tant qu'outil de gestion de conf, présente les caractéristiques suivantes :
* **libre et opensource**
* outil en **CLI**
* développé en **Python**
* **repose sur le protocole SSH** (dans une utilisation classique) pour déployer la configuration de façon distante
  * protocole très éprouvé et extrêmement mature
* agentless : pas besoin d'installation particulière sur les macines de destination
  * simplement un serveur SSH + la configuration adéquate
  * et Python installé
* les fichiers Ansible sont au **format YAML**
* le langage Ansible est un **langage déclaratif**
  * on ne décrit pas comment faire une action
  * mais uniquement l'état souhaité de la machine
  * *par exemple on écrira "le paquet `vim` doit être installé" (déclaratif) et non pas "lance la commande `yum install vim`" (exécutif)*

> Nous ne l'aborderons pas ici, mais il existe des outils permettant de fournir une interface graphique à Ansible : [Ansible Tower](https://www.ansible.com/products/tower) pour la version enterprise, ou [AWX](https://github.com/ansible/awx) pour un outil libre et opensource. On peut aussi citer [Foreman](https://theforeman.org/) qui supporte une intégration Ansible.

# II. Why ? 

## 1. Pourquoi utiliser un outil de gestion de conf ?

Ce sont des outils puissants qui permettent de **déployer des choses simples, comme des choses très complexes avec une métohde de déploiement unifiée**. Ces outils permettent justement de garder la complexité de nos infrastructures sous contrôle.

De tels outils **augmentent aussi grandement le niveau de maintenabilité** d'une infrastructure donnée.  
En effet, une fois en place et utilisé pour tous les déploiements, un outil de gestion conf rend compt ede façon exhaustive de ce qu'il est possible de déployer dans l'infra. On parle parfois de *self-documentation* : l'ensemble de la conf hébergée dans notre outil de gestion de conf est une doc de l'infra.  
On peut aussi avoir une liste exhaustive des machines présentes dans le parc, avec la liste exacte de ce qu'il y a dessus.  

**Moins de shellscripts !**. Les langages de scripting comme Powershell, Bash etc. sont des outils très puissants. Mais bien souvent, ces scripts sont difficlement maintenables et subissent douloureusement les affres du temps.

La **centralisation de la configuration** apporte des bénéfices en soit :
* toutes les configurations peuvent être sauvegardées depuis un endroit unique, puisque toutes hébergées au même endroit
* l'accès au configuration est unifié
  * tous les admins utilisent les mêmes outils et techniques pour faire leur job

Il n'y a **plus de connexions manuelles sur les machines**.  
Sauf en cas d'incident, de nécessité de troubleshoot un comportement anormal, etc. il n'est jamais nécessaire de se connecter directement à une machine pour l'administret : on se contente de tout déployer à distance.

Etant donné qu'on écrit donc une description de toute notre infra dans des fichiers texte au format YAML, on peut parler **d'Infrastructure as Code** (IaC). [Voir la section dédiée, plus bas](#4-infrastructure-as-code-).

## 2. Pourquoi choisir Ansible ? 

Ansible présente les avantages suivants :
* **agentless + Python + SSH**
  * pas besoin de configurations particulières ou d'installation lourde : Ansible repose sur des outils standards
* **YAML**
  * qu'on aime ou non, YAML est un langage de stockage de donnée standardisé, simple à écrire manuellement
* **communauté**
  * beaucoup beaucoup de retours d'expérience et ca d'utilisation
  * énormément de *modules* déjà développés par la communauté 
* **efficience**
  * Ansible est un outil performant et léger
* **flexibilité**, on peut aussi bien l'utiliser pour :
  * des actions one-shot, ponctuelles
    * à exécuter immédiatement sur un ou plusieurs noeuds
  * de la gestion de parc
    * on déploie la conf **tous les jours** pour s'assurer qu'elle n'a pas bougé
* **cross-platform**
  * Windows, GNU/Linux, BSD, autres
  * mais aussi matériel réseau comme Cisco, HP, autres
  * et bien plus encoooore

## 3. Des inconvénients ?

Ok c'est pas du sellscripting, mais ça **manque de rigueur naturelle**, en particulier Ansible.  
Ansible est si flexible et permissif qu'il est possible de créer des choses strictement inmaintenables avec, comme il est possible de proposer une gestion de parc avancée, claire, résiliente et prompte à subir des évolutions.  
L'utilisation de **bonnes pratiques** peut remédier à ce problème.

Aussi, une mise à niveau de la part des administrateurs, et l'utilisation de techniques de développement par ceux-ci sont nécessaires.  
Cela demande de repenser un peu la gestion de parc au quotidien.

## 4. Infrastructure as code ?

Toute notre infra est donc déclarée dans des fichiers textes... Comme du code. On va donc pouvoir utiliser les techniques ises en place par les développeurs pour gérer leur code, mais adapté à nos outils. On parle alors de **code d'infrastructure**. 

On peut alors partir sur :
* **versionning** du code, en mettant tout ça dans git
* mise en place de **tests**
* **intégration et déploiement continu** pour augmenter le niveau de qualité global de la gestion d'infra, et améliorer l'agilité de cette gestion

Aussi, encore une fois, la sauvegarde de toute l'infra tient dans une poignée de fichiers texte, facilitant ainsi grandement le processus.

# III. Ansible

Pour le vocabulaire et l'utilisation élémentaire de Ansible, voir [l'intro du mini-TP lié à Ansible](../../tp/3/ansible/README.md#intro).

# IV. Ansible good practices

## 1. Gestion de conf de façon générale

> A venir.

## 2. Ansible

> A venir.

## 3. Un mot sur le contrôle de conformité

> A venir.
