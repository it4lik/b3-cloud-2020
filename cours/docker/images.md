# Containers & Docker

- [Containers & Docker](#containers--docker)
- [Images Docker](#images-docker)
  - [Concept](#concept)
  - [Tags](#tags)
  - [Docker Hub](#docker-hub)
  - [Dépôt library](#d%c3%a9p%c3%b4t-library)
  - [Créer une image](#cr%c3%a9er-une-image)

# Images Docker

## Concept

Les *images* sont constituées de :
* un filesystem
  * un ensemble structuré de dossiers et fichiers
* des métadonnées
  * des informations sur l'image elle-même
    * nom
    * version
    * etc.

C'est donc simplement l'ensemble de fichiers qu'utilisent un conteneur.  

Une fois qu'un conteneur est démarré, il utilise les fichiers de l'image sur laquelle il se base en Read-Only.  
S'il écrit un nouveau fichier, modifie ou supprime un fichier existant, alors il écrira dans un espace qui lui est réservé.  

**Une image reste toujours intacte, elle est immuable**. Ainsi, plusieurs conteneurs peuvent utiliser la même image.

## Tags

On peut utiliser des *tags* sur les images, afin de préciser différentes versions d'une image. On les écrit après le nom de l'image : `<IMAGE>:<TAG>`.

Par exemple, [l'image MySQL](https://hub.docker.com/_/mysql) du Hub possède, entre autres, les images
* `mysql:5.6`
* `mysql:5.7`
* `mysql:8`

## Docker Hub

Il existe un répertoire public d'images destinées à être utilisées par Docker : le Docker Hub.  

Il est possible de l'explorer avec un navigateur web : https://hub.docker.com/

Le fonctionnement d'un registre Docker est analogue à un Gitlab/Github : chaque image est exposée sous un compte spécifique (comme chaque projet sur Gitlab/Github est hébergé par un compte spécifique).  

Par exemple : `https://hub.docker.com/r/freeipa/freeipa-server` correspond à l'image `freeipa-server` hébergée par le compte `freeipa`.  

## Dépôt library

Il existe un dépôt particulier sur le Docker Hub appelé *library*. Il contient des images vérifiées par Docker Hub (sécurité, bonnes pratiques, etc.) et publiées directement par les éditeurs des solutions. Les images qui s'y trouvent sont désormais appelées "docker official images".
Les images du dépôt *library* sont hébergées sous l'URL `https://hub.docker.com/_/` (notez l'underscore à la place d'un nom d'utilisateur)

Par exemple Alpine est une image de *library* : https://hub.docker.com/_/alpine.

> A noter qu'il est parfaitement possible d'installer et configurer un registre privé. En utilisant simplement un [registry](https://github.com/docker/distribution) ou avec des solutions plus abouties qui embarquent un registry comme Portus ou Harbor.

## Créer une image

Afin de créer une image, on se base la plupart du temps sur une image existante. En utilisant le contenu existant, on peut ajouter ou modifier des applications dans l'image.

> "La plupart du temps" car il est aussi possible de créer une image from scratch, mais on verra ça plus tard.

Afin de créer une image, il est nécessaire de rédiger un *Dockerfile* ; puis d'appeler la commande `docker build` qui construira une image.

```
Dockerfile ---build---> Image
```

Un *Dockerfile* possède une [syntaxe standardisée](https://docs.docker.com/engine/reference/builder/) où l'on trouve souvent au minimum :

```
# Image de base
FROM alpine

# Ajout d'une application dans le conteneur
RUN apk update && apk add nginx

# Définition d'un processus à lancer lorsque le conteneur démarre
CMD ["nginx" "-g" "daemon off;"]
```
