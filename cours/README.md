# Supports de Cours

* Docker
  * [Slides du cours](./docker/slides-containerization.html)
  * [Notion d'images Docker](./docker/images.md)
* Vagrant
  * [Vagrant et repackaging](./vagrant/)
* Ansible
  * [Notion de gestion de conf et présentation d'Ansible](./ansible/)
