# Setup Vagrant environment

- [Setup Vagrant environment](#setup-vagrant-environment)
  - [Obvious (or is it ?)](#obvious-or-is-it)
  - [Création d'un box de base](#cr%c3%a9ation-dun-box-de-base)
  - [Vagrantfile TP](#vagrantfile-tp)

## Obvious (or is it ?)

* install Vagrant
* install d'un hyperviseur (VirtualBox ?)
* éventuelle config intermédiaire
* test du bon fonctionnement

```bash
$ mkdir test
$ cd test
$ vagrant init centos/7

# Cela génère un fichier Vagrantfile dans le dossier courant
# Vous pouvez le pimp un peu pour qu'il soit encore plus léger
# Par exemple

$ cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end
end

# Test
$ vagrant up
[...]
$ vagrant ssh
```

## Création d'un box de base

On va setup une box qui contient CentOS7, avec docker pré-installé et configuré. Le but étant d'accélérer les tests pendant le TP, et mettre un peu les mains dans le provisionnement automatisée de machines virtuelles.

```ruby
$ mkdir package
$ cd package
$ vagrant init centos/7

# Configurez le Vagrantfile de façon minimaliste
$ cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true

    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end
end

# On peut allumer la VM
$ vagrant up
```

L'idée est d'utiliser cette VM lancée avec Vagrant, installer et configurer des trucs dedans, puis en resortir et la définir comme une nouvelle box de base.  
On pourra alors instancier de nouvelles VM à partir de cette box créée de nos mains :). 

```bash
# Let's gooooo
$ vagrant ssh

# CE QUI SUIT SE PASSE DANS LA VM

# On suit la doc d'install de Docker
$ sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
$ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
$ sudo yum install -y docker-ce

# Activation du démon docker au démarrage de la machine
$ sudo systemctl enable docker

# Ajout de l'utilisateur 'vagrant' au groupe 'docker'
# Cela permet, pour rappel, d'utiliser les commandes docker sans avoir besoin des droits de root (avec sudo par exemple)
$ sudo usermod -aG docker vagrant

# Et c'est touuut
# On clean vite fait not' merdier
$ rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
$ yum clean all
$ rm -rf /tmp/*
$ rm -f /var/log/wtmp /var/log/btmp
$ history -c

$ exit
```

Okay on a notre VM qui est prête ! Now, on la transforme en box Vagrant.

```bash
# Sur l'hôte, toujours dans le dossier du Vagrantfile. On a pas bougé quoi.
$ vagrant package --output centos7-docker.box
$ vagrant box add centos7-docker centos7-docker.box

# Pour vérifier
$ vagrant box list
```

Maintenant on peut se servir de la box nouvellement créée pour créer des VMs.

C'est à dire, dans un fichier Vagrantfile, on pourra désormais utiliser, par exemple :
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "centos7-docker"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true

    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end
end
```
